package org.openstreetmap.josm.plugins.surveyvalidator;

import static org.openstreetmap.josm.tools.I18n.tr;

import org.openstreetmap.josm.plugins.surveyvalidator.actions.*;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.openstreetmap.josm.actions.AutoScaleAction;
import org.openstreetmap.josm.actions.AutoScaleAction.AutoScaleMode;
import org.openstreetmap.josm.data.osm.DataSelectionListener;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.DataSelectionListener.SelectionChangeEvent;
import org.openstreetmap.josm.data.osm.event.SelectionEventManager;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.SideButton;
import org.openstreetmap.josm.gui.dialogs.ToggleDialog;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerAddEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerChangeListener;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerOrderChangeEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerRemoveEvent;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.gui.widgets.ListPopupMenu;
import org.openstreetmap.josm.gui.widgets.PopupMenuLauncher;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

public class SurveyValidatorDialog extends ToggleDialog {

	private static final long serialVersionUID = 5237880161893005707L;

	private final DefaultListSelectionModel selectionModel = new DefaultListSelectionModel();
	private final ListModel model = new ListModel(selectionModel);
	private final JList<ListItem> jList = new JList<>(model);
	private final ImportAction actImport = new ImportAction(model);
	private final NavigateAction actMoveTopPrev = new NavigateAction("prev", model);
	private final NavigateAction actMoveTopNext = new NavigateAction("next", model);
	private final ValidateAction actVal = new ValidateAction();
	//private final CloseListAction actRemove = new CloseListAction();

	public SurveyValidatorDialog() {
		super(tr("SurveyValidator"), "todo", tr("Open Survey validator."), Shortcut.registerShortcut("subwindow:todo",
				tr("Toggle: {0}", tr("Todo list")), KeyEvent.VK_T, Shortcut.CTRL_SHIFT), 150);
		buildContentPanel();
	}

	/**
	 * Builds the content panel for this dialog
	 */
	protected void buildContentPanel() {
		jList.setSelectionModel(selectionModel);
		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jList.setCellRenderer(new ListItemRenderer());
		jList.setTransferHandler(null);

		SideButton valButton = new SideButton(actVal);
		SideButton prevButton = new SideButton(actMoveTopPrev);
		SideButton nextButton = new SideButton(actMoveTopNext);
		SideButton importButton = new SideButton(actImport);
		//SideButton closeListButton = new SideButton(actRemove);

		createLayout(jList, true,
	Arrays.asList(new SideButton[] { importButton, valButton, prevButton, nextButton, /*closeListButton*/ }));
	}
}
