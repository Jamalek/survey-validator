package org.openstreetmap.josm.plugins.surveyvalidator;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.openstreetmap.josm.data.osm.DefaultNameFormatter;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Logging;

public class ListItemRenderer implements ListCellRenderer<ListItem> {
    private final DefaultNameFormatter formatter = DefaultNameFormatter.getInstance();
    private final DefaultListCellRenderer defaultListCellRenderer = new DefaultListCellRenderer();

    @Override
    public Component getListCellRendererComponent(JList<? extends ListItem> list, ListItem value, int index,
            boolean isSelected, boolean cellHasFocus) {
        Component def = defaultListCellRenderer.getListCellRendererComponent(list, null, index, isSelected, cellHasFocus);
//        boolean fast = list.getModel().getSize() > 1000;

        if (value != null && def instanceof JLabel) {
            String displayName = tr("Imported feature #");
            ((JLabel) def).setText(displayName);
//            final ImageIcon icon = fast
//                    ? ImageProvider.get(value.primitive.getType())
//                    : ImageProvider.getPadded(value.primitive,
//                        // Height of component no yet known, assume the default 16px.
//                        ImageProvider.ImageSizes.SMALLICON.getImageDimension());
//            if (icon != null) {
//                ((JLabel) def).setIcon(icon);
//            } else {
//                Logging.warn("Null icon for "+value.primitive.getDisplayType());
//            }
//            ((JLabel) def).setToolTipText(formatter.buildDefaultToolTip(value.primitive));
        }
        return def;
    }
}
