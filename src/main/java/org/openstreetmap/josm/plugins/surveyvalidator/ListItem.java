package org.openstreetmap.josm.plugins.surveyvalidator;

import java.util.HashMap;

public class ListItem {
	private static String[] headerValues;
	private final HashMap<String, String> attrs;
	public final double lat;
	public final double lon;

	public ListItem(double lat, double lon, HashMap<String, String> attrs) {
		this.lat = lat;
		this.lon = lon;
		this.attrs = attrs;
	}

	public static void setHeader(String[] headerValues) {
		ListItem.headerValues = headerValues;
	}

	public static ListItem parse(String[] values) {
		HashMap<String, String> attrs = new HashMap<>();
		double lat = 0;
		double lon = 0;
		for (int i = 0; i < values.length; i++) {
			String value = values[i];
			String headerValue = headerValues[i];
			if (SurveyAttributes.redundantAttributes.contains(headerValue)) {
				//Immediatelly skip all attributes which we know to be useless
				continue;
			} else if (SurveyAttributes.attributeMappings.containsKey(headerValue)) {
				if (SurveyAttributes.attributeMappings.get(headerValue).equals("lat")) {
					lat = Double.parseDouble(value);
				} else if (SurveyAttributes.attributeMappings.get(headerValue).equals("lon")) {
					lon = Double.parseDouble(value);
				} else {
					// Fill in known and meaningfull attributes from file
					attrs.put(SurveyAttributes.attributeMappings.get(headerValue), value);
				}
			} else {
				//#TODO: dialog window with "manual alignment tool"
				//System.out.println("unknown attribute");
			}
		}
		return new ListItem(lat, lon, attrs);
	}

	public static String[] getHeaderValues() {
		return headerValues;
	}

	public String getAttribute(String attrName) {
		return attrs.get(attrName);
	}

	public HashMap<String, String> getAttributes() {
		return attrs;
	}
}
