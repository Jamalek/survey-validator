package org.openstreetmap.josm.plugins.surveyvalidator.actions;

import static org.openstreetmap.josm.tools.I18n.tr;

import org.openstreetmap.josm.plugins.surveyvalidator.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashMap;

import javax.swing.AbstractAction;

import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.plugins.surveyvalidator.ListModel;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.gui.MainApplication;

public class NavigateAction extends AbstractAction {
	private String direction;

	private static ListModel model;

	private static int modelIndex = -1;

	public NavigateAction(String direction, ListModel model) {
		this.direction = direction;
		if (direction.equals("prev")) {
			putValue(NAME, tr("Previous POI"));
			putValue(SHORT_DESCRIPTION, tr("Add the selected items to the todo list."));
			new ImageProvider("dialogs", "previous").getResource().attachImageIcon(this, true);
		} else {
			putValue(NAME, tr("Next POI"));
			putValue(SHORT_DESCRIPTION, tr("Add the selected items to the todo list."));
			new ImageProvider("dialogs", "next").getResource().attachImageIcon(this, true);
		}
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (direction.equals("prev")) {
				// System.out.println("moving to prev point");
				if (modelIndex > 0) {
					modelIndex--;

				}
			} else {
				if (modelIndex < model.getSize() - 1) {
					modelIndex++;
					// System.out.println("moving to next point");
				}
			}

			OsmDataLayer odl = MainApplication.getLayerManager().getActiveDataLayer();

			if (modelIndex < model.getSize()) {
        ListItem item = model.getElementAt(modelIndex);
				LatLon point = new LatLon(item.lat, item.lon);
				Node n = new Node(point);
        HashMap<String, String> attrs = item.getAttributes();
        Object[] keys = attrs.keySet().toArray();
        for (int i = 0; i < keys.length; i++) {
            n.put(keys[i].toString(), attrs.get(keys[i]));
            n.put("fixme", "POI imported from field survey and not yet validated!");
        }
				odl.data.addPrimitive(n);
				zoomToPoint(point);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void zoomToPoint(LatLon point) {
		MainApplication.getMap().mapView.zoomTo(new Bounds(point.getY()-0.001, point.getX()-0.001, point.getY()+0.001, point.getX()+0.001));
	}
}
