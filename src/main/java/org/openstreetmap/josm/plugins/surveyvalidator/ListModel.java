package org.openstreetmap.josm.plugins.surveyvalidator;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListSelectionModel;

public class ListModel extends AbstractListModel<ListItem> {

	private final ArrayList<ListItem> todoList = new ArrayList<>();
	private final ArrayList<ListItem> doneList = new ArrayList<>();
	private final DefaultListSelectionModel selectionModel;

	public ListModel(DefaultListSelectionModel selectionModel) {
		this.selectionModel = selectionModel;
	}

	@Override
	public ListItem getElementAt(int index) {
		return todoList.get(index);
	}

	@Override
	public int getSize() {
		return todoList.size();
	}

    public void addItems(Collection<ListItem> items) {
        if (items == null || items.isEmpty())
            return;
        doneList.removeAll(items);
        int size = getSize();
        if (size == 0) {
            todoList.addAll(items);
            super.fireIntervalAdded(this, 0, getSize()-1);
            selectionModel.setSelectionInterval(0, 0);
        } else {
            todoList.ensureCapacity(size + items.size());
            for (ListItem item: items) {
                if (!todoList.contains(item))
                    todoList.add(item);
            }
            super.fireIntervalAdded(this, size, getSize()-1);
        }
    }
}
