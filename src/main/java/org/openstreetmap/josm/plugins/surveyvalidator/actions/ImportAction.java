package org.openstreetmap.josm.plugins.surveyvalidator.actions;

import static org.openstreetmap.josm.tools.I18n.tr;

import org.openstreetmap.josm.plugins.surveyvalidator.*;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.openstreetmap.josm.actions.AutoScaleAction;
import org.openstreetmap.josm.actions.AutoScaleAction.AutoScaleMode;
import org.openstreetmap.josm.actions.downloadtasks.DownloadOsmTask;
import org.openstreetmap.josm.actions.downloadtasks.DownloadParams;
import org.openstreetmap.josm.actions.downloadtasks.PostDownloadHandler;
import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.Data;
import org.openstreetmap.josm.data.DataSource;
import org.openstreetmap.josm.data.osm.DataSelectionListener;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.DataSelectionListener.SelectionChangeEvent;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.event.SelectionEventManager;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.SideButton;
import org.openstreetmap.josm.gui.dialogs.ToggleDialog;
import org.openstreetmap.josm.gui.download.DownloadSettings;
import org.openstreetmap.josm.gui.download.OverpassDownloadSource;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerAddEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerChangeListener;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerOrderChangeEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerRemoveEvent;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.gui.progress.AbstractProgressMonitor;
import org.openstreetmap.josm.gui.progress.CancelHandler;
import org.openstreetmap.josm.gui.progress.ProgressMonitor;
import org.openstreetmap.josm.gui.progress.ProgressTaskId;
import org.openstreetmap.josm.gui.widgets.ListPopupMenu;
import org.openstreetmap.josm.gui.widgets.PopupMenuLauncher;
import org.openstreetmap.josm.io.MultiFetchServerObjectReader;
import org.openstreetmap.josm.io.OsmTransferException;
import org.openstreetmap.josm.io.OverpassDownloadReader;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

public class ImportAction extends AbstractAction {

	private final ListModel model;

	public ImportAction(ListModel model) {
		this.model = model;
		putValue(NAME, tr("Import"));
		putValue(SHORT_DESCRIPTION, tr("Add the selected items to the todo list."));
		new ImageProvider("dialogs", "add").getResource().attachImageIcon(this, true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ignored) {
		}
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle(tr("Select a file for import"));
		jfc.setAcceptAllFileFilterUsed(false);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV", "csv");
		jfc.addChoosableFileFilter(filter);
		int returnValue = jfc.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File selectedFile = jfc.getSelectedFile();
			importCsv(selectedFile.getAbsolutePath());
		}
	}

	private static final double MAX_DISTANCE_DEGREES = 0.001; //equals roughly to 300m

	private void importCsv(String absolutePath) {
		List<ListItem> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(absolutePath))) {
			String line;
			ListItem.setHeader(br.readLine().replace("\"", "").split(";"));
			while ((line = br.readLine()) != null) {
				String[] values = line.replace("\"", "").split(";");
				try {
					records.add(ListItem.parse(values));
				} catch (Exception ignored) {
				}
			}
		} catch (Exception ignored) {
		}
		Bounds downloadBounds = null;
		model.addItems(records);
		for (int i = 0; i < 10/*records.size()*/; i++) {
			ListItem record = records.get(i);
			downloadBounds = new Bounds(record.lat - MAX_DISTANCE_DEGREES, record.lon - MAX_DISTANCE_DEGREES,
					record.lat + MAX_DISTANCE_DEGREES, record.lon + MAX_DISTANCE_DEGREES);

			final DownloadOsmTask task = new DownloadOsmTask();
			final Future<?> future = task.download(new DownloadParams(), downloadBounds, null);
			MainApplication.worker.submit(new Runnable() {
				@Override
				public void run() {
					try {
						future.get();
					} catch (Exception e) {
					}
				}
			});
		}



//			OverpassDownloadReader downloadReader = new OverpassDownloadReader(downloadBounds, OverpassDownloadReader.OVERPASS_SERVER.get(), "");
//			downloadReader.fetchData(api, subtask, parser, monitor, reason)
//
//			DownloadSettings settings = new DownloadSettings(downloadBounds, false, false);
//			Data data = new Data() {
//				@Override
//				public Collection<DataSource> getDataSources() {
//					return Arrays.asList(new DataSource[] {new DataSource(downloadBounds, "OpenStreetMap Server")});
//				}
//			};
//	        Bounds area = settings.getDownloadBounds().orElse(new Bounds(0, 0, 0, 0));
//	        DownloadOsmTask task = new DownloadOsmTask();
//	        task.setZoomAfterDownload(settings.zoomToData());
//	        Future<?> future = task.download(
//	                new OverpassDownloadReader(area, OverpassDownloadReader.OVERPASS_SERVER.get(), data.getQuery()),
//	                new DownloadParams().withNewLayer(settings.asNewLayer()), area, null);
//	        OverpassDownloadSource source = new OverpassDownloadSource();
//	        MainApplication.worker.submit(new PostDownloadHandler(task, future, data.getErrorReporter()));

//		DownloadOsmTask task = new DownloadOsmTask();
//		DownloadParams downloadParams = new DownloadParams();
//		ProgressMonitor progressMonitor = createProgressMonitor();
//		task.download(downloadParams, downloadBounds, progressMonitor);


//		runDownload();
	}

	private boolean canceled;
    private MultiFetchServerObjectReader objectReader;

	private void runDownload() {
        try {
            DataSet d = new DataSet();
            OsmDataLayer odl = new OsmDataLayer(d, "vrstva", null);
            MainApplication.getLayerManager().addLayer(odl);
            synchronized (this) {
                if (canceled) return;
                objectReader = MultiFetchServerObjectReader.create();
            }
//            objectReader.append(children);
            ProgressMonitor progressMonitor = createProgressMonitor();
            final DataSet dataSet = objectReader.parseOsm(progressMonitor.createSubTaskMonitor(ProgressMonitor.ALL_TICKS, false));
            if (dataSet == null)
                return;
            dataSet.deleteInvisible();
            synchronized (this) {
                if (canceled) return;
                objectReader = null;
            }

            SwingUtilities.invokeLater(() -> {
            	odl.mergeFrom(dataSet);
            	odl.onPostDownloadFromServer();
            });
        } catch (OsmTransferException e) {
        }
	}

	private AbstractProgressMonitor createProgressMonitor() {
		return new AbstractProgressMonitor(null) {
			@Override
			public void setProgressTaskId(ProgressTaskId var1) {
				// TODO Auto-generated method stub

			}

			@Override
			public ProgressTaskId getProgressTaskId() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Component getWindowParent() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected void doBeginTask() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void doFinishTask() {
				// TODO Auto-generated method stub

			}

			@Override
			protected void doSetIntermediate(boolean var1) {
//				this.cancel();
			}

			@Override
			protected void doSetTitle(String var1) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void doSetCustomText(String var1) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void updateProgress(double var1) {
				// TODO Auto-generated method stub
			}
		};
	}
}
