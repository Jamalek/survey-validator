package org.openstreetmap.josm.plugins.surveyvalidator;

import java.util.ArrayList;
import java.util.HashMap;

public class SurveyAttributes {

  public static HashMap<String, String> attributeMappings;
  public static ArrayList<String> redundantAttributes;

  // #TODO: load from attributeMappings.yml or alike
  static {
    attributeMappings = new HashMap<>();
    attributeMappings.put("source", "source");
    attributeMappings.put("source:date", "source:date");
    attributeMappings.put("lat", "lat");
    attributeMappings.put("_location_latitude", "lat");
    attributeMappings.put("_Geolocation_latitude", "lat");
    attributeMappings.put("lon", "lon");
    attributeMappings.put("_location_longitude", "lon");
    attributeMappings.put("_Geolocation_longitude", "lon");
    attributeMappings.put("_location_altitude", "ele");
    attributeMappings.put("name", "name");
    attributeMappings.put("alt_name", "alt_name");
    attributeMappings.put("amenity", "amenity");
    attributeMappings.put("dispensing", "dispensing");
    attributeMappings.put("is_in:health_zone", "is_in:health_zone");
    attributeMappings.put("is_in:health_area", "is_in:health_area");
    attributeMappings.put("note", "note");
    attributeMappings.put("Comentários", "note");

    //#TODO: load from .tagignore or alike
    redundantAttributes = new ArrayList<>();
    redundantAttributes.add("start");
    redundantAttributes.add("end");
    redundantAttributes.add("today");
    redundantAttributes.add("simserial");
    redundantAttributes.add("subscriberid");
    redundantAttributes.add("deviceid");
  }
}
