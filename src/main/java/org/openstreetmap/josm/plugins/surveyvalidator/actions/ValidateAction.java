package org.openstreetmap.josm.plugins.surveyvalidator.actions;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.openstreetmap.josm.actions.ZoomInAction;
import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.plugins.surveyvalidator.ListModel;
import org.openstreetmap.josm.tools.ImageProvider;
//import sun.applet.Main;

public class ValidateAction extends AbstractAction {
  public ValidateAction() {
    putValue(NAME, tr("Approve"));
    putValue(SHORT_DESCRIPTION, tr("Add the selected items to the todo list."));
    new ImageProvider("dialogs", "validator").getResource().attachImageIcon(this, true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    System.out.println("foo");
  }
}
